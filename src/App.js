import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { withAuthenticator } from "aws-amplify-react";

import "./App.css";
import Routes from "./route/Routes";
import Header from "./layout/Header";

const signUpConfig = {
  header: "Sign Up",
  hideAllDefaults: true,
  defaultCountryCode: "61",
  signUpFields: [
    {
      label: "First Name",
      key: "given_name",
      required: true,
      displayOrder: 1,
      type: "string"
    },
    {
      label: "Last Name",
      key: "family_name",
      required: true,
      displayOrder: 2,
      type: "string"
    },
    {
      label: "Username",
      key: "username",
      required: true,
      displayOrder: 3,
      type: "string"
    },
    {
      label: "Email",
      key: "email",
      required: true,
      displayOrder: 4,
      type: "string"
    },
    {
      label: "Password",
      key: "password",
      required: true,
      displayOrder: 5,
      type: "password"
    },
    {
      label: "PhoneNumber",
      key: "phone_number",
      required: true,
      displayOrder: 6,
      type: "string"
    }
  ]
};

function App() {
  return (
    <div>
      <Router>
        <Header />
        <Routes />
      </Router>
    </div>
  );
}

export default withAuthenticator(App, { signUpConfig });
