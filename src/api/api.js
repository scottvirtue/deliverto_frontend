import axios from "axios";

import AuthMiddleware from "./authMiddleware";

const coreApiUrl = process.env.REACT_APP_CORE_API_URL;
const analyticsApiUrl = process.env.REACT_APP_ANALYTICS_API_URL;

const API = {
  users: {
    get: async () => {
      await AuthMiddleware();
      var result = await axios.get(`${coreApiUrl}/api/Users`);
      return result;
    },
    create: async user => {
      await AuthMiddleware();
      return await axios.post(`${coreApiUrl}/api/Users`, user);
    },
    getReceivers: async () => {
      await AuthMiddleware();
      return await axios.get(`${coreApiUrl}/api/Users/receivers/list`);
    }
  },
  deliveries: {
    create: async delivery => {
      await AuthMiddleware();
      return await axios.post(`${coreApiUrl}/api/Deliveries`, delivery);
    },
    update: async delivery => {
      await AuthMiddleware();
      return await axios.put(`${coreApiUrl}/api/Deliveries`, delivery);
    },
    getAll: async () => {
      await AuthMiddleware();
      return await axios.get(`${coreApiUrl}/api/Deliveries/list`);
    }
  },
  analytics: {
    getUsers: async () => {
      await AuthMiddleware();
      return await axios.get(`${analyticsApiUrl}/api/Analytics/users/list`);
    },
    getDeliveries: async () => {
      await AuthMiddleware();
      return await axios.get(
        `${analyticsApiUrl}/api/Analytics/deliveries/list`
      );
    }
  }
};

export default API;
