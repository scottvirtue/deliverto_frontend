import { Auth } from "aws-amplify";
import axios from "axios";

async function AuthMiddleware() {
  const token = (await Auth.currentSession()).getIdToken().jwtToken;
  console.log(token);
  axios.defaults.headers.common = {
    Authorization: `Bearer ${token}`
  };
}

export default AuthMiddleware;
