import React from "react";
import styled from "styled-components";
import {
  Col,
  Spinner,
  Form,
  Label,
  Button,
  FormGroup,
  Input
} from "reactstrap";

import moment from "moment";

const CreateDeliveryView = props => {
  return (
    <Wrapper>
      <h3>
        Create Delivery View -{" "}
        <span style={{ fontWeight: "bold" }}>
          {props.loggedInUser.firstName} {props.loggedInUser.lastName}
        </span>{" "}
      </h3>
      {props.serverRequest ? (
        <div style={{ textAlign: "center", padding: "5em" }}>
          <Spinner
            style={{
              width: "3rem",
              height: "3rem"
            }}
          />
        </div>
      ) : (
        <Col sm={6}>
          <Form
            style={{ marginTop: "50px" }}
            onSubmit={e => {
              e.preventDefault();
              props.createDelivery();
            }}
          >
            <FormGroup row>
              <Label for="selectedMerchant" sm={3}>
                Merchant *
              </Label>
              <Col sm={9}>
                <select
                  disabled={props.serverRequest}
                  required
                  id="merchant"
                  className="form-control"
                  value={props.delivery.merchant}
                  onChange={props.handleChange}
                  name="merchant"
                >
                  <option value="">
                    {props.serverRequest ? "Loading..." : "Please Select"}
                  </option>
                  {props.merchants.map((merchant, index) => {
                    return (
                      <option key={index} value={merchant}>
                        {merchant}
                      </option>
                    );
                  })}
                </select>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label for="products" sm={3}>
                Number of products *
              </Label>
              <Col sm={9}>
                <Input
                  required
                  min="1"
                  step="1"
                  value={
                    props.delivery.products > 0 ? props.delivery.products : ""
                  }
                  onChange={props.handleChange}
                  type="number"
                  name="products"
                  id="products"
                  placeholder="Enter a number of products"
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label for="selectedTransportProvider" sm={3}>
                Transport Provider *
              </Label>
              <Col sm={9}>
                <select
                  disabled={props.serverRequest}
                  required
                  id="selectedTransportProvider"
                  className="form-control"
                  value={props.delivery.transportProvider}
                  onChange={props.handleChange}
                  name="transportProvider"
                >
                  <option value="">
                    {props.serverRequest ? "Loading..." : "Please Select"}
                  </option>
                  {props.transportProviders.map((transportProvider, index) => {
                    return (
                      <option key={index} value={transportProvider}>
                        {transportProvider}
                      </option>
                    );
                  })}
                </select>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label for="selectedReceiver" sm={3}>
                Receiver *
              </Label>
              <Col sm={9}>
                <select
                  disabled={props.serverRequest}
                  required
                  id="receiverId"
                  className="form-control"
                  value={props.delivery.receiverId}
                  onChange={props.handleChange}
                  name="receiverId"
                >
                  <option value="">
                    {props.serverRequest ? "Loading..." : "Please Select"}
                  </option>

                  {props.receivers.map((receiver, index) => {
                    return (
                      <option key={receiver.username} value={receiver.username}>
                        {`${receiver.firstName} ${receiver.lastName} @ ${receiver.location.address}`}
                      </option>
                    );
                  })}
                </select>
              </Col>
            </FormGroup>
            <Button
              disabled={props.serverRequest}
              style={{ marginTop: "2em", float: "right" }}
              color="info"
            >
              {props.serverRequest ? <Spinner /> : "Create"}
            </Button>
          </Form>
        </Col>
      )}
    </Wrapper>
  );
};

const Wrapper = styled.div`
  margin: 3em 3em 3em 3em;
  label {
    font-weight: bold;
  }
`;

export default CreateDeliveryView;
