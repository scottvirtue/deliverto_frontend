import React, { Component } from "react";
import CreateDeliveryView from "./CreateDeliveryView";
import API from "../../api/api";
import moment from "moment";

class CreateDeliveryContainer extends Component {
  state = {
    serverRequest: false,
    loggedInUser: {},
    receivers: [],
    transportProviders: [
      "Australia Post",
      "Couries Please",
      "Startrack",
      "TNT"
    ],
    merchants: ["Daivd Jones", "Myer", "Nike", "Adidas"],
    delivery: {
      merchant: "",
      transportProvider: "",
      expectedDeliveryDate: "",
      products: 0,
      receiverId: 0
    }
  };

  async componentDidMount() {
    this.setState(
      {
        ...this.state,
        serverRequest: true
      },
      async () => {
        const [loggedInUser, receivers] = await Promise.all([
          API.users.get(),
          API.users.getReceivers()
        ]);

        this.setState(
          {
            ...this.state,
            serverRequest: false,
            loggedInUser: loggedInUser.data,
            receivers: receivers.data
          },
          () => {
            console.log("create delivery data", this.state);
          }
        );
      }
    );
  }

  handleChange = event => {
    const model = Object.assign({}, this.state.delivery);

    model[event.target.name] = event.target.value;

    this.setState({ ...this.state, delivery: model });
  };

  createDelivery = async () => {
    this.setState(
      {
        ...this.state,
        serverRequest: true
      },
      async () => {
        const selectedReceiver = this.state.receivers.find(
          x => x.username === this.state.delivery.receiverId
        );
        let newDelivery = {
          buyerUsername: this.state.loggedInUser.username,
          receiverUsername: selectedReceiver.username,
          buyer: this.state.loggedInUser,
          receiver: selectedReceiver,
          expectedDeliveryDate: moment().format("YYYY-MM-DD HH:mm:ss"),
          merchantName: this.state.delivery.merchant,
          transportProviderName: this.state.delivery.transportProvider,
          products: this.state.delivery.products.toString()
        };

        var result = await API.deliveries.create(newDelivery);
        console.log("Created Delivery!", result);
        setTimeout(() => {
          this.setState(
            {
              ...this.state,
              serverRequest: false,
              delivery: {
                merchant: "",
                transportProvider: "",
                expectedDeliveryDate: "",
                products: 0,
                receiverId: 0
              }
            },
            () => {
              this.props.history.push("/my-deliveries");
              console.log("cleared state", this.state.delivery);
            }
          );
        }, 6000);
      }
    );
  };

  render() {
    return (
      <CreateDeliveryView
        {...this.state}
        handleChange={this.handleChange}
        createDelivery={this.createDelivery}
      />
    );
  }
}

export default CreateDeliveryContainer;
