import React from "react";
import styled from "styled-components";
import {
  Card,
  CardBody,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  Table,
  Spinner,
  Button
} from "reactstrap";
import classnames from "classnames";
import moment from "moment";

const AnalyticsView = props => {
  return (
    <Wrapper>
      <h3>Analytics View</h3>
      <Card>
        <CardBody>
          <TabWrapper>
            <Nav tabs>
              <NavItem>
                <NavLink
                  className={classnames({
                    active: props.activeTab === "user-analytics"
                  })}
                  onClick={() => {
                    props.toggle("user-analytics");
                  }}
                >
                  <span> User Analytics </span>
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({
                    active: props.activeTab === "deliveries-analytics"
                  })}
                  onClick={() => {
                    props.toggle("deliveries-analytics");
                  }}
                >
                  <span> Delivery Analytics </span>
                </NavLink>
              </NavItem>
            </Nav>
            <TabContent activeTab={props.activeTab}>
              <TabPane tabId="user-analytics">
                {" "}
                {props.serverRequest ? (
                  <div style={{ textAlign: "center", padding: "5em" }}>
                    <Spinner
                      style={{
                        width: "3rem",
                        height: "3rem"
                      }}
                    />
                  </div>
                ) : (
                  <Table>
                    <thead>
                      <tr>
                        <th>User Name</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Type</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>Address</th>
                      </tr>
                    </thead>
                    <tbody>
                      {props.userAnalytics &&
                        props.userAnalytics.length > 0 &&
                        props.userAnalytics.map((user, index) => {
                          return (
                            <tr key={user.username}>
                              <th scope="row">{user.username}</th>
                              <td>{user.firstName}</td>
                              <td>{user.lastName}</td>
                              <td>
                                {user.type === "Buyer" ? (
                                  <Button
                                    style={{
                                      width: "100%",
                                      pointerEvents: "none"
                                    }}
                                    color="danger"
                                    size="sm"
                                  >
                                    {user.type}
                                  </Button>
                                ) : (
                                  <Button
                                    style={{
                                      width: "100%",
                                      pointerEvents: "none"
                                    }}
                                    color="info"
                                    size="sm"
                                  >
                                    {user.type}
                                  </Button>
                                )}
                              </td>
                              <td>{user.email}</td>
                              <td>{user.phoneNumber}</td>
                              <td>{user.location && user.location.address}</td>
                            </tr>
                          );
                        })}
                    </tbody>
                  </Table>
                )}
              </TabPane>
              <TabPane tabId="deliveries-analytics">
                {" "}
                {props.serverRequest ? (
                  <div style={{ textAlign: "center", padding: "5em" }}>
                    <Spinner
                      style={{
                        width: "3rem",
                        height: "3rem"
                      }}
                    />
                  </div>
                ) : (
                  <Table>
                    <thead>
                      <tr>
                        <th>Id</th>
                        <th>Buyer</th>
                        <th>Receiver</th>
                        <th>Receiver Address</th>
                        <th>Status</th>
                        <th>Merchant</th>
                        <th>Transport Provider</th>
                        <th>Products</th>
                        <th>Exp. Delivery Date</th>
                        <th>Act. Delivery Date</th>
                      </tr>
                    </thead>
                    <tbody>
                      {props.deliveryAnalytics &&
                        props.deliveryAnalytics.length > 0 &&
                        props.deliveryAnalytics.map((delivery, index) => {
                          return (
                            <tr key={delivery.id}>
                              <th scope="row">{delivery.id}</th>

                              <td>{`${delivery.buyer.firstName} ${delivery.buyer.lastName}`}</td>
                              <td>{`${delivery.receiver.firstName} ${delivery.receiver.lastName}`}</td>
                              <td>{`${delivery.receiver.location.address}`}</td>
                              <td>
                                {delivery.deliveryDate ? (
                                  <Button
                                    style={{
                                      width: "100%",
                                      pointerEvents: "none"
                                    }}
                                    color="success"
                                    size="sm"
                                  >
                                    Received
                                  </Button>
                                ) : (
                                  <Button
                                    style={{
                                      width: "100%",
                                      pointerEvents: "none"
                                    }}
                                    color="warning"
                                    size="sm"
                                  >
                                    In Progress
                                  </Button>
                                )}
                              </td>
                              <td>{delivery.merchantName}</td>
                              <td>{delivery.transportProviderName}</td>
                              <td>{delivery.products}</td>
                              <td>
                                {moment(
                                  new Date(delivery.expectedDeliveryDate)
                                ).format("MMMM Do YYYY, h:mm:ss a")}
                              </td>
                              <td>
                                {delivery.deliveryDate &&
                                  moment(
                                    new Date(delivery.deliveryDate)
                                  ).format("MMMM Do YYYY, h:mm:ss a")}
                              </td>
                            </tr>
                          );
                        })}
                    </tbody>
                  </Table>
                )}
              </TabPane>
            </TabContent>
          </TabWrapper>
        </CardBody>
      </Card>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  margin: 3em 3em 3em 3em;
`;

const TabWrapper = styled.div`
  width: 100%;
  background-color: white;
  .nav-item {
    cursor: pointer;
    width: 50%;
    text-align: center;
    font-size: 14px;
    font-weight: 300;
    a.active {
      border-bottom: 5px solid #22b9ff;
      span {
        font-weight: 600;
      }
    }
  }
`;

export default AnalyticsView;
