import React, { Component } from "react";
import AnalyticsView from "./AnalyticsView";
import API from "../../api/api";

class AnalyticsContainer extends Component {
  state = {
    serverRequest: false,
    activeTab: "user-analytics",
    userAnalytics: [],
    deliveryAnalytics: []
  };

  toggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  };

  async componentDidMount() {
    this.setState(
      {
        ...this.state,
        serverRequest: true
      },
      async () => {
        const [userAnalytics, deliveryAnalytics] = await Promise.all([
          API.analytics.getUsers(),
          API.analytics.getDeliveries()
        ]);
        this.setState({
          ...this.state,
          serverRequest: false,
          userAnalytics: userAnalytics.data,
          deliveryAnalytics: deliveryAnalytics.data
        });
      }
    );
  }

  render() {
    return <AnalyticsView {...this.state} toggle={this.toggle} />;
  }
}

export default AnalyticsContainer;
