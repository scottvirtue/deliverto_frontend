import React, { Component } from "react";
import MyDeliveriesView from "./MyDeliveriesView";
import API from "../../api/api";

class MyDeliveriesContainer extends Component {
  state = {
    serverRequest: false,
    myDeliveries: []
  };

  async componentDidMount() {
    this.setState(
      {
        ...this.state,
        serverRequest: true
      },
      async () => {
        let result = await API.deliveries.getAll();
        this.setState({
          ...this.state,
          serverRequest: false,
          myDeliveries: result.data
        });
      }
    );
  }

  render() {
    return <MyDeliveriesView {...this.state} />;
  }
}

export default MyDeliveriesContainer;
