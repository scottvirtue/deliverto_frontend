import React from "react";
import styled from "styled-components";
import { Table, Spinner, Button } from "reactstrap";
import moment from "moment";

const MyDeliveriesView = props => {
  return (
    <Wrapper>
      <h3>My Deliveries View</h3>
      {props.serverRequest ? (
        <div style={{ textAlign: "center", padding: "5em" }}>
          <Spinner
            style={{
              width: "3rem",
              height: "3rem"
            }}
          />
        </div>
      ) : (
        <Table>
          <thead>
            <tr>
              <th>Id</th>
              <th>Buyer</th>
              <th>Receiver</th>
              <th>Receiver Address</th>
              <th>Status</th>
              <th>Merchant</th>
              <th>Transport Provider</th>
              <th>Products</th>
              <th>Exp. Delivery Date</th>
              <th>Act. Delivery Date</th>
            </tr>
          </thead>
          <tbody>
            {props.myDeliveries &&
              props.myDeliveries.length > 0 &&
              props.myDeliveries.map((delivery, index) => {
                return (
                  <tr key={delivery.id}>
                    <th scope="row">{delivery.id}</th>

                    <td>{`${delivery.buyer.firstName} ${delivery.buyer.lastName}`}</td>
                    <td>{`${delivery.receiver.firstName} ${delivery.receiver.lastName}`}</td>
                    <td>{`${delivery.receiver.location.address}`}</td>
                    <td>
                      {delivery.deliveryDate ? (
                        <Button
                          style={{ width: "100%", pointerEvents: "none" }}
                          color="success"
                          size="sm"
                        >
                          Received
                        </Button>
                      ) : (
                        <Button
                          style={{ width: "100%", pointerEvents: "none" }}
                          color="warning"
                          size="sm"
                        >
                          In Progress
                        </Button>
                      )}
                    </td>
                    <td>{delivery.merchantName}</td>
                    <td>{delivery.transportProviderName}</td>
                    <td>{delivery.products}</td>
                    <td>
                      {moment(new Date(delivery.expectedDeliveryDate)).format(
                        "MMMM Do YYYY, h:mm:ss a"
                      )}
                    </td>
                    <td>
                      {delivery.deliveryDate &&
                        moment(new Date(delivery.deliveryDate)).format(
                          "MMMM Do YYYY, h:mm:ss a"
                        )}
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </Table>
      )}
    </Wrapper>
  );
};

const Wrapper = styled.div`
  margin: 3em 3em 3em 3em;
`;

export default MyDeliveriesView;
