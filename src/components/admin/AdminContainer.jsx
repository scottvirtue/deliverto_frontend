import React, { Component } from "react";
import AdminView from "./AdminView";
import API from "../../api/api";
import moment from "moment";

class AdminContainer extends Component {
  state = {
    serverRequest: false,
    serverRequestCreate: false,
    newReceiver: {
      firstName: "",
      lastName: "",
      email: "",
      phoneNumber: "",
      address: "",
      city: "",
      state: "",
      postcode: ""
    },
    loggedInUser: {
      firstName: "",
      lastName: "",
      email: "",
      phoneNumber: "",
      address: "",
      city: "",
      state: "",
      postcode: ""
    },
    loggedInUsersPendingDeliveries: []
  };

  async componentDidMount() {
    await this.init();
  }

  async init() {
    this.setState({ ...this.state, serverRequest: true }, async () => {
      const [loggedInUser, loggedInUsersDeliveries] = await Promise.all([
        API.users.get(),
        API.deliveries.getAll()
      ]);

      const loggedInUsersPendingDeliveries = loggedInUsersDeliveries.data.filter(
        delivery => !delivery.deliveryDate
      );
      console.log(
        "loggedInUsersPendingDeliveries",
        loggedInUsersDeliveries.data
      );

      this.setState({
        ...this.state,
        loggedInUser: loggedInUser.data,
        loggedInUsersPendingDeliveries: loggedInUsersPendingDeliveries,
        serverRequest: false
      });
    });
  }

  handleChange = event => {
    const model = Object.assign({}, this.state.newReceiver);

    model[event.target.name] = event.target.value;

    this.setState({ ...this.state, newReceiver: model });
  };

  createReceiver = async () => {
    let newReceiver = this.state.newReceiver;
    this.setState({ ...this.state, serverRequestCreate: true }, async () => {
      let result = await API.users.create({
        ...newReceiver,
        type: "Receiver",
        location: {
          address: `${newReceiver.address} ${newReceiver.city} ${newReceiver.state} ${newReceiver.postcode}`
        }
      });
      console.log("Created Receiver!", result);
      this.setState(
        {
          ...this.state,
          serverRequestCreate: false,
          newReceiver: {
            firstName: "",
            lastName: "",
            email: "",
            phoneNumber: "",
            address: "",
            city: "",
            state: "",
            postcode: ""
          }
        },
        () => {
          console.log("cleared state", this.state.newReceiver);
        }
      );
    });
  };

  deliver = async id => {
    this.setState({ ...this.state, serverRequest: true }, async () => {
      const deliveryToUpdate = this.state.loggedInUsersPendingDeliveries.find(
        delivery => delivery.id === id
      );
      deliveryToUpdate.deliveryDate = moment().format("YYYY-MM-DD HH:mm:ss");
      let result = await API.deliveries.update(deliveryToUpdate);
      console.log("Delivery Updated!", result);
      setTimeout(async () => {
        this.setState(
          {
            ...this.state,
            newReceiver: {
              firstName: "",
              lastName: "",
              email: "",
              phoneNumber: "",
              address: "",
              city: "",
              state: "",
              postcode: ""
            }
          },
          async () => {
            console.log("cleared state", this.state.newReceiver);
            await this.init();
          }
        );
      }, 6000);
    });
  };

  render() {
    return (
      <AdminView
        {...this.state}
        handleChange={this.handleChange}
        createReceiver={this.createReceiver}
        deliver={this.deliver}
      />
    );
  }
}

export default AdminContainer;
