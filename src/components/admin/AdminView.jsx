import React from "react";
import styled from "styled-components";
import {
  Col,
  Row,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Card,
  CardBody,
  CardTitle,
  Spinner,
  Table
} from "reactstrap";
const AdminView = props => {
  return (
    <Wrapper>
      <h3>Admin View</h3>
      <Card body inverse color="primary">
        <h4>Logged In User - Details</h4>

        {props.serverRequest ? (
          <div style={{ textAlign: "center", padding: "1em" }}>
            <Spinner
              style={{
                width: "3rem",
                height: "3rem"
              }}
            />
          </div>
        ) : (
          <Row form>
            <Col md={6}>
              <FormGroup>
                <Label for="">
                  <span style={{ fontWeight: "bold" }}>First Name:</span>{" "}
                  {props.loggedInUser.firstName}
                </Label>
              </FormGroup>
              <FormGroup>
                <Label for="">
                  <span style={{ fontWeight: "bold" }}>Last Name:</span>{" "}
                  {props.loggedInUser.lastName}
                </Label>
              </FormGroup>
              <FormGroup>
                <Label for="">
                  <span style={{ fontWeight: "bold" }}>Username:</span>{" "}
                  {props.loggedInUser.username}
                </Label>
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Label for="">
                  <span style={{ fontWeight: "bold" }}>Email:</span>{" "}
                  {props.loggedInUser.email}
                </Label>
              </FormGroup>
              <FormGroup>
                <Label for="">
                  <span style={{ fontWeight: "bold" }}>Phone Number:</span>{" "}
                  {props.loggedInUser.phoneNumber}
                </Label>
              </FormGroup>
              <FormGroup>
                <Label for="">
                  <span style={{ fontWeight: "bold" }}>Type:</span>{" "}
                  {props.loggedInUser.type}
                </Label>
              </FormGroup>
            </Col>
            <h4>Logged In User - Pending Deliveries</h4>
            <Table>
              <thead>
                <tr>
                  <th>Id</th>

                  <th>Receiver</th>
                  <th>Receiver Address</th>
                  <th>Status</th>
                  <th>Merchant</th>
                  <th>Transport Provider</th>
                  <th>Products</th>
                  <th>Expected Delivery Date</th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {props.loggedInUsersPendingDeliveries &&
                  props.loggedInUsersPendingDeliveries.length > 0 &&
                  props.loggedInUsersPendingDeliveries.map(
                    (delivery, index) => {
                      return (
                        <tr key={delivery.id}>
                          <th scope="row">{delivery.id}</th>

                          <td>{`${delivery.receiver.firstName} ${delivery.receiver.lastName}`}</td>
                          <td>{`${delivery.receiver.location.address}`}</td>
                          <td>
                            {delivery.deliveryDate ? (
                              <Button
                                style={{ width: "100%", pointerEvents: "none" }}
                                color="success"
                                size="sm"
                              >
                                Received
                              </Button>
                            ) : (
                              <Button
                                style={{ width: "100%", pointerEvents: "none" }}
                                color="warning"
                                size="sm"
                              >
                                In Progress
                              </Button>
                            )}
                          </td>
                          <td>{delivery.merchantName}</td>
                          <td>{delivery.transportProviderName}</td>
                          <td>{delivery.products}</td>
                          <td>{delivery.expectedDeliveryDate}</td>

                          <td>
                            <Button
                              onClick={() => props.deliver(delivery.id)}
                              color="success"
                            >
                              Deliver
                            </Button>
                          </td>
                        </tr>
                      );
                    }
                  )}
              </tbody>
            </Table>
          </Row>
        )}
      </Card>

      <Card>
        <CardBody>
          <CardTitle>
            <span style={{ fontWeight: "bold" }}>Create Receiver</span>
          </CardTitle>
          <Form
            onSubmit={e => {
              e.preventDefault();
              props.createReceiver();
            }}
          >
            <Row form>
              <Col md={6}>
                <FormGroup>
                  <Label for="firstName">First Name</Label>
                  <Input
                    value={props.newReceiver.firstName}
                    onChange={props.handleChange}
                    required
                    type="text"
                    name="firstName"
                    id="firstName"
                  />
                </FormGroup>
              </Col>
              <Col md={6}>
                <FormGroup>
                  <Label for="lastName">Last Name</Label>
                  <Input
                    value={props.newReceiver.lastName}
                    onChange={props.handleChange}
                    required
                    type="text"
                    name="lastName"
                    id="lastName"
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row form>
              <Col md={6}>
                <FormGroup>
                  <Label for="email">Email</Label>
                  <Input
                    value={props.newReceiver.email}
                    onChange={props.handleChange}
                    required
                    type="email"
                    name="email"
                    id="email"
                  />
                </FormGroup>
              </Col>
              <Col md={6}>
                <FormGroup>
                  <Label for="phoneNumber">Phone Number</Label>
                  <Input
                    value={props.newReceiver.phoneNumber}
                    onChange={props.handleChange}
                    required
                    type="text"
                    name="phoneNumber"
                    id="phoneNumber"
                  />
                </FormGroup>
              </Col>
            </Row>
            <FormGroup>
              <Label for="address">Address</Label>
              <Input
                value={props.newReceiver.address}
                onChange={props.handleChange}
                required
                type="text"
                name="address"
                id="address"
              />
            </FormGroup>
            <Row form>
              <Col md={6}>
                <FormGroup>
                  <Label for="city">City</Label>
                  <Input
                    value={props.newReceiver.city}
                    onChange={props.handleChange}
                    required
                    type="text"
                    name="city"
                    id="city"
                  />
                </FormGroup>
              </Col>
              <Col md={4}>
                <FormGroup>
                  <Label for="state">State</Label>
                  <Input
                    value={props.newReceiver.state}
                    onChange={props.handleChange}
                    required
                    type="text"
                    name="state"
                    id="state"
                  />
                </FormGroup>
              </Col>
              <Col md={2}>
                <FormGroup>
                  <Label for="postcode">Postcode</Label>
                  <Input
                    value={props.newReceiver.Postcode}
                    onChange={props.handleChange}
                    required
                    type="text"
                    name="postcode"
                    id="postcode"
                  />
                </FormGroup>
              </Col>
            </Row>
            <Button disabled={props.serverRequestCreate} color="primary">
              {props.serverRequestCreate ? <Spinner /> : "Create"}
            </Button>
          </Form>
        </CardBody>
      </Card>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  margin: 3em 3em 3em 3em;
  th,
  td {
    color: white;
  }
`;

export default AdminView;
