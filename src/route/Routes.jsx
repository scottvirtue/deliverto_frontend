import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import AdminContainer from "../components/admin/AdminContainer";
import AnalyticsContainer from "../components/analytics/AnalyticsContainer";
import DeliveriesContainer from "../components/my-deliveries/MyDeliveriesContainer";
import CreateDeliveryContainer from "../components/create-delivery/CreateDeliveryContainer";
class Routes extends Component {
  render() {
    return (
      <Switch>
        <Route path="/" exact={true} component={AdminContainer} />
        <Route
          path="/create-delivery"
          exact={true}
          component={CreateDeliveryContainer}
        />
        <Route
          path="/my-deliveries"
          exact={true}
          component={DeliveriesContainer}
        />
        <Route path="/analytics" exact={true} component={AnalyticsContainer} />
        <Redirect from="*" to="/" />
      </Switch>
    );
  }
}

export default Routes;
