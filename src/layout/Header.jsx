import React from "react";
import { NavLink as NavLinkReactRouter } from "react-router-dom";
import { Nav, NavItem, NavLink, NavbarBrand, Navbar } from "reactstrap";
import styled from "styled-components";
import { Greetings } from "aws-amplify-react";

export default class Example extends React.Component {
  render() {
    return (
      <React.Fragment>
        <div style={{ display: "flex" }}>
          <Navbar
            color="dark"
            light
            expand="md"
            style={{ padding: "1.5em", flexGrow: "2" }}
          >
            <NavbarBrand href="/">
              <span style={{ color: "#979A9A", fontWeight: 800 }}>
                DeliverTo
              </span>
            </NavbarBrand>
            <Nav pills fill>
              <NavItem>
                <NavItemStyled>
                  <NavLink
                    tag={NavLinkReactRouter}
                    exact
                    to="/"
                    activeClassName="active"
                  >
                    Admin
                  </NavLink>
                </NavItemStyled>
              </NavItem>
              <NavItem>
                <NavItemStyled>
                  <NavLink
                    tag={NavLinkReactRouter}
                    exact
                    to="/analytics"
                    activeClassName="active"
                  >
                    Analytics
                  </NavLink>
                </NavItemStyled>
              </NavItem>

              <NavItem>
                <NavItemStyled>
                  <NavLink
                    tag={NavLinkReactRouter}
                    exact
                    to="/my-deliveries"
                    activeClassName="active"
                  >
                    My Deliveries
                  </NavLink>
                </NavItemStyled>
              </NavItem>
              <NavItem>
                <NavItemStyled>
                  <NavLink
                    tag={NavLinkReactRouter}
                    exact
                    to="/create-delivery"
                    activeClassName="active"
                  >
                    Create Delivery
                  </NavLink>
                </NavItemStyled>
              </NavItem>
            </Nav>
          </Navbar>
          <GreetingStyle>
            <Greetings />
          </GreetingStyle>
        </div>
      </React.Fragment>
    );
  }
}
const GreetingStyle = styled.div`
  padding: 10px 10px 0px 0px;
  background-color: #343a40;
  > div {
    background-color: #343a40;
    border: none;
    color: white;
  }
`;

const NavItemStyled = styled.div`
  padding-right: 1em;
  a.nav-link {
    color: white;
    span {
      font-size: 14px;
      font-weight: 400;
    }
  }
  a.nav-link.active {
    background-color: #22b9ff;
    color: white;
  }
`;
